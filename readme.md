# Mastering the Hello
---

[Site Preview](https://hello-513e8.firebaseapp.com/)

This project is built on the Foundation 6 framework using the foundation-cli and accompanying tooling around Gulp.js (for more info see the 'ZURB Template' section below). 

Additional libraries and plugins: 

- [ScrollMagic for scroll-based animations](http://scrollmagic.io/)
- [Greensock for animations](http://greensock.com/)
- [Swiper.js for carousels](http://idangero.us/swiper/)
- [vidbg for the header bkg video](https://github.com/blakewilson/vidbg)
- [audioplayer](http://osvaldas.info/audio-player-responsive-and-touch-friendly)

## Up and running

After cloning the Bitbucket repo you'll want to cd into the project directory, then run `npm install` and `bower install` to get all the required libraries downloaded. Then you should be able to run `foundation watch` to initiate all the Gulp tasks and automatically launch a local server with live reload at http://localhost:8000/. Make sure your local version of npm matches the versiion specified in package.json. Use [nvm](https://github.com/creationix/nvm) to move between npm version locally.

_Note: these steps assume you already have node/npm, bower, and the foundation cli installed._

## Deploying to the staging server

Use the firebase-cli, configured to push the /dist directory. 

Run `firebase deploy` to log in and push updates to the staging server. 


---

# ZURB Template

[![devDependency Status](https://david-dm.org/zurb/foundation-zurb-template/dev-status.svg)](https://david-dm.org/zurb/foundation-zurb-template#info=devDependencies)

**Please open all issues with this template on the main [Foundation for Sites](https://github.com/zurb/foundation-sites/issues) repo.**

This is the official ZURB Template for use with [Foundation for Sites](http://foundation.zurb.com/sites). We use this template at ZURB to deliver static code to our clients. It has a Gulp-powered build system with these features:

- Handlebars HTML templates with Panini
- Sass compilation and prefixing
- JavaScript concatenation
- Built-in BrowserSync server
- For production builds:
  - CSS compression
  - JavaScript compression
  - Image compression

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (0.12 or greater)
- [Git](https://git-scm.com/)

This template can be installed with the Foundation CLI, or downloaded and set up manually.

### Using the CLI

Install the Foundation CLI with this command:

```bash
npm install foundation-cli --global
```

Use this command to set up a blank Foundation for Sites project with this template:

```bash
foundation new --framework sites --template zurb
```

The CLI will prompt you to give your project a name. The template will be downloaded into a folder with this name.

### Manual Setup

To manually set up the template, first download it with Git:

```bash
git clone https://github.com/zurb/foundation-zurb-template projectname
```

Then open the folder in your command line, and install the needed dependencies:

```bash
cd projectname
npm install
bower install
```

Finally, run `npm start` to run Gulp. Your finished site will be created in a folder called `dist`, viewable at this URL:

```
http://localhost:8000
```

To create compressed, production-ready assets, run `npm run build`.
