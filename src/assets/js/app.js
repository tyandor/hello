$(document).foundation();

// Background Video
$('.vidbg-box').vidbg({
  'mp4': 'assets/mov/bkg-video.mp4',
  'webm': 'assets/mov/bkg-video.webm',
  'poster': 'assets/mov/bkg-video.jpg'
}, {
  volume: 0,
  playbackRate: 1,
  muted: true,
  loop: true,
  position: '50% 50%',
  resizing: true,
  overlay: false
});

// Swiper Three Step
var swiper = new Swiper('.swiper-three-step', {
    pagination: '.pagination-three-step',
    paginationClickable: true,
    nextButton: '.button-next-three-step',
    prevButton: '.button-prev-three-step',
    onlyExternal: true,
    parallax: true,
    speed: 900,
    slideToClickedSlide: true,
    keyboardControl: true,
    draggable: false,
    preventClicks: true,
    loop: true
});

// scrollmagic animations
var controller = new ScrollMagic.Controller();

// fade header-content
var header = new ScrollMagic.Scene({
        triggerElement: "#trigger-opacity",
        duration: 700
    })
    .setTween(".header-content", {opacity: 0})
    .addTo(controller);

// intro opacity (optimize this with a timeline)
var tweenOpacity = TweenMax.to(".overlay", 1, {className: "+=opaque"});

var scene = new ScrollMagic.Scene({triggerElement: "#intro-content", duration: 500, offset: 50})
                .setTween(tweenOpacity)
                .addTo(controller);

// intro zoom
var scene0 = new ScrollMagic.Scene({
        triggerElement: "#intro-content",
        duration: 500,
        offset: 50
    })
    .setTween(".intro-bkg", {scale: 1.5, className: "+=blur"})
    .addTo(controller);

// init audio player
$( 'audio' ).audioPlayer(
    {
        classPrefix: 'audioplayer',
        strPlay: 'Play',
        strPause: 'Pause',
        strVolume: 'Volume'
    }
);

// scale image - meet-allison
var scene1 = new ScrollMagic.Scene({
        triggerElement: "#meet-allison",
        duration: 500,
        offset: -50
    })
    .setTween(".img-round1", {scale: 1, opacity: 1})
    .addTo(controller);

// scale image - how did that go
var scene2 = new ScrollMagic.Scene({
        triggerElement: "#how-did-that-go",
        duration: 500,
        offset: -50
    })
    .setTween(".img-round2", {scale: 1, opacity: 1})
    .addTo(controller);

// wwjd toggles
$( "#wwjd1" ).on( "click", function() {
    $("body, html").animate({
        scrollTop: $("#wwjd-scroll-to").offset().top -150
    }, 250);
    $(".ai .card").toggleClass('flipped');
    $(this).toggleClass('active');
    $(".arc .card").removeClass('flipped');
    $(".akq .card").removeClass('flipped');
    $("#wwjd2, #wwjd3").removeClass('active');
    $( "#wwjd2, #wwjd3").text(function(i, text){
        return text === "+" ? "+" : "+";
    });
    $(this).text(function(i, text){
        return text === "+" ? "-" : "+";
    });
});

$( "#wwjd2" ).on( "click", function() {
    $("body, html").animate({
        scrollTop: $("#wwjd-scroll-to").offset().top -150
    }, 250);
    $(".arc .card").toggleClass('flipped');
    $(this).toggleClass('active');
    $(".ai .card").removeClass('flipped');
    $(".akq .card").removeClass('flipped');
    $("#wwjd1, #wwjd3").removeClass('active');
    $( "#wwjd1, #wwjd3").text(function(i, text){
        return text === "+" ? "+" : "+";
    });
    $(this).text(function(i, text){
        return text === "+" ? "-" : "+";
    });
});

$( "#wwjd3" ).on( "click", function() {
    $("body, html").animate({
        scrollTop: $("#wwjd-scroll-to").offset().top -150
    }, 250);
    $(".akq .card").toggleClass('flipped');
    $(this).toggleClass('active');
    $(".ai .card").removeClass('flipped');
    $(".arc .card").removeClass('flipped');
    $("#wwjd1, #wwjd2").removeClass('active');
    $( "#wwjd1, #wwjd2").text(function(i, text){
        return text === "+" ? "+" : "+";
    });
    $(this).text(function(i, text){
        return text === "+" ? "-" : "+";
    });
});

// all-together opacity (optimize this with a timeline)
var tweenOpacity = TweenMax.to(".all-together-overlay", 1, {className: "+=opaque"});

var scene = new ScrollMagic.Scene({triggerElement: "#all-together-content", duration: 500, offset: -50})
                .setTween(tweenOpacity)
                .addTo(controller);

// all-together zoom
var scene0 = new ScrollMagic.Scene({
        triggerElement: "#all-together-content",
        duration: 500,
        offset: -50
    })
    .setTween(".all-together-bkg", {scale: 1, className: "+=blur"})
    .addTo(controller);

